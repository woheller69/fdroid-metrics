#!/usr/bin/env python3

from urllib.request import urlretrieve
from urllib.error import HTTPError
import json
from os.path import isfile


servers = [
    "http01",
    "http02",
    "http03",
]

for serv in servers:
    print(f"Downloading index for {serv}")
    index_url = f"https://fdroid.gitlab.io/metrics/{serv}.fdroid.net/index.json"
    index_file = f"raw_data/{serv}/index.json"
    urlretrieve(index_url, index_file)

    with open(index_file, "r") as f:
        index_data = json.load(f)

    for date_json in index_data:
        # Note that date_json is of the form "2024-05-01.json",
        # i.e. it includes the ".json" suffix!
        date_url = f"https://fdroid.gitlab.io/metrics/{serv}.fdroid.net/{date_json}"
        date_file = f"raw_data/{serv}/{date_json}"

        if isfile(date_file):
            print(f"Skipping {serv}/{date_json} (file exists)")
            continue

        print(f"Downloading {serv}/{date_json}")
        try:
            urlretrieve(date_url, date_file)
        except HTTPError as e:
            print(f"[ERROR]: {e} for url={date_url}")
