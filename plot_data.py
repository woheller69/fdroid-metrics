#!/usr/bin/env python3

import json
import os
import glob

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

apps = [os.path.basename(f)[:-5] for f in sorted(glob.glob("app_downloads/*.json"))]
# print(apps)

# Thank you ChatGPT for helping me write this code

for app in apps:
    app_downloads_file = f"app_downloads/{app}.json"
    with open(app_downloads_file, "r") as f:
        app_downloads = json.load(f)

    x_values = list(app_downloads.keys())
    y_values = list(app_downloads.values())

    plt.figure(figsize=(10, 6))
    plt.plot(x_values, y_values, marker="o", linestyle="-")

    for i, (x, y) in enumerate(zip(x_values, y_values)):
        plt.annotate(str(y), xy=(x, y), xytext=(x, y), ha="center")

    # Rotate x-axis labels for better readability
    plt.xticks(rotation=45)
    # Always start the y-axis at 0
    plt.ylim(0)

    plt.xlabel("week")
    plt.ylabel("# downloads")
    plt.title(f"Number of downloads of {app}")
    plt.tight_layout()

    # https://stackoverflow.com/a/25973637/11076036
    plt.gca().yaxis.set_major_formatter(
        ticker.FuncFormatter(lambda x, p: format(int(x), ","))
    )

    png_file = f"graphs/{app}.png"
    print(f"saving {png_file}")
    plt.savefig(png_file)
