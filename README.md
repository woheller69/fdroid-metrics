# F-Droid Metrics

This repo contains some helper scripts to parse the F-Droid metrics.

Graphs of app download numbers can be found:

- for a small number of apps in the [graphs](graphs) directory,
- for a larger number of apps at https://divestos.org/pages/fdroid_stats
  (list maintained by [DivestOS](https://forum.f-droid.org/t/does-f-droid-provide-any-sort-of-stats/4346/69)), and
- for any other apps you can run the scripts yourself.

## Background

Background on metrics:

- https://f-droid.org/2021/03/01/fdroid-metrics-and-clean-insights.html
- https://gitlab.com/fdroid/metrics-data/
- https://fdroid.gitlab.io/metrics/

Background on the server architecture:

- https://gitlab.com/fdroid/wiki/-/wikis/Internal/Servers
- https://gitlab.com/fdroid/web-services

Inspired by https://github.com/obfusk/fdroid-misc-scripts/blob/master/scripts/create-graphs.py.

## Installation

Plotting graphs requires matplotlib:

```
apt install python3-matplotlib
```

## Usage

```
./download_data.py
./parse_data.py
./plot_data.py
```

## Contributing

Just make a merge request.

## License

This project is licensed under GPL-v3-or-later ([LICENSE](LICENSE)).

